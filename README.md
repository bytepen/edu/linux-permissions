# Linux Permissions

This is a simple write-up and project to help you learn Linux Permissions a bit better with the help of Docker!

## Write-Up

The write-up is automatically pushed to GitLab Pages and can be found at the following link:

https://bytepen.gitlab.io/linux-permissions

## Docker

The Docker container can be accessed by running the following command on any Docker enabled machine:

```
docker run -it bytepen/linux-permissions
```

Note that you may want to update the image from time to time with the following command:

```
docker pull bytepen/linux-permissions
```

More information about this can be found on the [Testing](https://bytepen.gitlab.io/linux-permissions/testing) page of the Write-Up.
