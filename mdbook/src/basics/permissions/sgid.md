# SGID (Set Group ID) Permission

In this section, we will cover what it generally means when something is given `sgid` permissions, providing nothing else is overriding it.

As with every permission, the contents of this section may be overriden by permissions of parent directories or other oddities discussed in the [`complexities`](../../complexities/index.md) section.

In the following example, only the `sgid` permission has been granted:

`------S---`

In the following example, `read`, `write`, and `execute` has been granted to user, group, and others in addition to the `sgid` permission:

`-rwxrwsrwx`

#### When Applied to Files

Coming soon...

#### When Applied to Directories

Coming soon...

