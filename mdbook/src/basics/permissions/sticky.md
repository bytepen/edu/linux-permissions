# Sticky Permission

In this section, we will cover what it generally means when something is given the `sticky` permission, provided nothing else is overring it.

As with every permission, the contents of this section may be overriden by permissions of the parent directories or other oddities discussed in the [`complexities`](../../complexities/index.md) sction.

In the following example, only the `sticky` permission has been granted:

`---------T`

In the following example, `read`, `write`, and `execute` has been granted to user, group, and others in addition to the `sticky` permission:

`-rwxrwxrwt`

This permission is also known as the `Restricted Deletion Flag` when applied to directories because it restricts the users who can move/rename or delete the contents of the directory.

#### When Applied to Files

Coming soon...

#### When Applied to Directories

Coming soon...
