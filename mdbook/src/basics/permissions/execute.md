# Execute Permission

In this section, we will cover what it generally means when something is given `execute` permissions, providing nothing else is overriding it.

As with every permission, the contents of this section may be overriden by permissions of parent directories or other oddities discussed in the [`complexities`](../../complexities/index.md) section.

In the following example, only the `execute` permission has been granted to the user, group, and others:

`---x--x--x`

## When Applied to Files

Coming soon...

## When Applied to Directories

Coming soon...
