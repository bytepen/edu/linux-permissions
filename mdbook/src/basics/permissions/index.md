# Permissions

Basic Linux Permissions can be broken down into the following six groups:

* [`read`](../permissions/read.md)
* [`write`](../permissions/write.md)
* [`execute`](../permissions/execute.md)
* [`suid`](../permissions/suid.md)
* [`sgid`](../permissions/sgid.md)
* [`sticky`](../permissions/sticky.md)
