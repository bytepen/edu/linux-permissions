# Write Permission

In this section, we will cover what it generally means when something is given `write` permissions, providing nothing else is overriding it.

As with every permission, the contents of this section may be overriden by permissions of parent directories or other oddities discussed in the [`complexities`](../../complexities/index.md) section.

In the following example, only the `write` permission has been granted to the user, group, and others:

`--w--w--w-`

## When Applied to Files

The `write` permission, in regards to files, means that the file can be written to by the applicable user.

In the example above, anyone should be able to write to the file. Additionally, they should be able to move and delete the file.

## When Applied to Directories

Coming soon...
