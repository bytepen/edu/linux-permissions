# SUID (Set User ID) Permission

In this section, we will cover what it generally means when something is given `suid` permissions, providing nothing else is overriding it.

As with every permission, the contents of this section may be overriden by permissions of parent directories or other oddities discussed in the [`complexities`](/../../complexities/index.md) section.

In the following example, only the `suid` permission has been granted:

`---S------`

If the following example, `read`, `write`, and `execute` has been granted to user, group, and others in addition to the `suid` permission:

`-rwsrwxrwx`

#### When Applied to Files

The `suid` permission, in regards to files, means that if the file can be executed by a user, it will be executed with the permissions of the owner of the file. In other words, if a file owned by `root` has the `suid` permission is executed by user `james`, it will actually run with the permissions/abilities of `root`. This can be a little confusing at first, so let's look at an example.

```
-rws---r-x 1 root root ... file
```

This file is owned by the user `root` and the group `root`. If the user `james` is only a part of the group `james`, he would fall under the `others` set of permissions. The `others` set has `read` and `execute`, so `james` should have no problems executing this file.

Let's say this program displays the EUID\* when run. If `james` were to execute this program, it would display `0`, which is the User ID of `root`. If this permission were not set, it would display the User ID of `james`, let's say `1000`.

#### When Applied to Directories

Coming soon...

#### Notes

\* The EUID (Effective User ID) is the ID of the user whose permissions/abilities are used in the execution of a program.
