# Read Permission

In this section, we will cover what it generally means when something is given `read` permissions, providing nothing else is overriding it.

As with every permission, the contents of this section may be overriden by permissions of parent directories or other oddities discussed in the [`complexities`](../../complexities/index.md) section.

In the following example, only the `read` permission has been granted to the user, group, and others:

`-r--r--r--`

## When Applied to Files

The `read` permission, in regards to files, means that the contents of the file can be viewed by the applicable user. In the example above, anyone should be able to view the contents of the file.

## When Applied to Directories

The `read` permission, in regards to directories, means that the contents of the directory can be seen. In the example above, anyone should be able to view the contents of a folder. Trying to perform an `ls -l` command on a directory with only the `read` permission may not be incredibly useful as only the names will be shown as follows:

```
/LP/random $ ls -l brown
ls: brown/magenta: Permission denied
ls: brown/charcoal: Permission denied
ls: brown/raspberry.sh: Permission denied
ls: brown/..: Permission denied
ls: brown/pear.elf: Permission denied
ls: brown/.: Permission denied
ls: brown/purple: Permission denied
total 0  
```
