# Group Permissions Set

The permissions in the `group` permission set are applied to the users in the group who owns a given file. An example of a file where only the `group` set has full permissions can be seen in the `ls -l` output below:

```
----rwx--- 1 james audi 0 Mar 21 15:23 /tmp/test.txt
```

In this example, the group `audi` has been given read (`r`), write (`w`), and execute (`x`) permissions on the file `/tmp/test.txt`. Both the `user` and the `other` sets have not been given any permissions. This means that only `users` in the `audi` group (and root) can do anything to this file.

Note that the user `james` is unlikely to be able to do anything to this file, even if he is part of the `audi` group. This is explained in depth in the [`complexities`](/complexities/set-order.md) section of this material.
