# Other Permissions Set

The permission in the `other` permission set are applied to users who do not fall within the `user` or `group` sets. An example of a file where only the `other` set has full permissions can be seen in the `ls -l` output below:

```
-------rwx 1 james audi 0 Mar 21 15:23 /tmp/test.txt
```

In this example, the `other` set has been given read (`r`), write (`w`), and execute (`x`) permissions on the file `/tmp/test.txt`. Both the `user` and `group` sets have not been given any permissions. This means that only `users` that are not `james` and not part of the `audi` group (and the `root` user) can do anything to this file.

Note that `james` and users who are part of the `audi` group are unlikely to be able to do anything. This is explained in depth in the [`complexities`](/complexities/set-order.md) section of this material.
