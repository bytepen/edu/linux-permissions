# Sets

When looking at linux permissions, you will find they are broken into three groups; [User](sets/user.md), [Group](sets/group.md), and [Other](sets/other.md).

More information about each of these groups can be found on their respective pages, however, we will briefly cover all of them here.

## Example

Take a look at the following line generated with the `ls -l` command:

```
-rw-r--r-- 1 user group 0 Mar 21 15:23 /tmp/test.txt
```

Let's drop off the additional information so we just have the permissions:

```
rw-r--r--
```

*As a side note, the first dash we dropped off specifies the type of file (regular, directory, link, etc) and is not directly related to permissions*

If we take these characters and break them apart into groups of three, then relate them to our sets, we can see the following permissions:

* User: `rw-`
    * The user specified in the full line is able to `read` from and `write` to this file. They cannot `execute` it.
* Group: `r--`
    * Members of the group specified in the first line can only `read` from this file. They cannot `write` to or `execute` it.
    * Be aware that this includes users who have this as their `primary` group or any `supplementary` groups.
* Other: `r--`
    * Any users that are not covered by the first two groups can `read` from this file. They cannot `write` or `execute` it.
