# User Permissions Set

The permissions in the `user` permission set are applied to the user who owns a given file. An example of a file where only the `user` set has full permissions can be seen in the `ls -l` output below:

```
-rwx------ 1 james audi 0 Mar 21 15:23 /tmp/test.txt
```

In this example, the user `james` has been given read (`r`), write (`w`), and execute (`x`) permissions on the file `/tmp/test.txt`. Both the `group` and the `other` sets have not been given any permissions. This means that the only user (aside from root) that can do anything to this file is `james`.


