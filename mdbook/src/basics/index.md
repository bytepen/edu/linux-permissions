# Basics

When it comes down to it, most people are just worried about three permissions in regards to Linux. These permissions are [`read`](./permissions/read.md), [`write`](./permissions/write.md), and [`execute`](./permissions/execute.md). These permissions are applied to three sets of users. These the the [`user`](./sets/user.md) who owns the file, the [`group`](./sets/group.md) that owns the file, and the [`other`](./sets/other.md) users who do not fall into the first two groups.

Additionally, there are three permissions people often have a hard time remembering. These permissions are as follows:
* The [`suid`](./permissions/suid.md) bit, which allows the file to be used as if it were used by the user that owns the file
* The [`sgid`](./permissions/sgid.md) bit, which allows the file to be used as if it were used by the group that owns the file
* The [`sticky`](./permissions/sticky.md) bit, which can be a bit harder to understand, but generally affords certain permissions to the owner of a file, while providing limited permissions to others.

All of these permissions and sets will be explained better in their respective sections of this material.
