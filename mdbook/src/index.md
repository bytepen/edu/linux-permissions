# Linux Permissions

Whether you're here because you know you don't know Linux Permissions as well as you would like to or you believe that there is nothing more to learn because it's 'only' read, write, and execute, I'm sure you'll pick up some knowledge and I'm glad your here!

## Learning

### Basics

We will start off with the [basics](./basics/index.md).

First, we will cover the three main permissions:
* [read (`r`)](./basics/permissions/read.md)
* [write (`w`)](./basics/permissions/write.md)
* [execute (`x`)](./basics/permissions/execute.md)

Then, we will cover the additional permissions:
* [Set User ID (SUID)](./basics/permissions/suid.md)
* [Set Group ID (SUID)](./basics/permissions/sgid.md)
* [Sticky Bit](./basics/permissions/sticky.md)

Finally, we will cover the sets of permissions:
* [User](./basics/sets/user.md)
* [Group](./basics/sets/group.md)
* [Other](./basics/sets/other.md)

### Access Control Lists

After we get the basics down, we will throw [Access Control Lists](./acls/index.md) into the mix.

### Complexities

Once we get an understanding of how all these permissions tie together, we will dig into some of the [complexities](./complexities/index.md) that can arise with Linux Permissions.

For example, can `james`, who is only in the group `james`, execute the following `folder/file.sh`? 

```
drwxrw---- root james folder
-rwxrwxrwx james root file.sh
```

If you don't know that the answer is `no`, then this section is defintely worth checking out.

## Testing

Once you believe you have a grasp on Linux Permissions, I encourage you to check out the Docker Image I've put together to help test your knowledge. For more information about this, check out the [Testing](./testing.md) page.
