# Set Order Complexities

Let's take a look at the following file:

```
----rwxr-- 1 james audi 0 Mar 21 15:23 /tmp/test.txt
```

It is worth knowing that permissions are evaluated from left to right. In other words, they are evaluated in the order of `user`, `group`, then `other`. If a user matches on an earier set, their permissions match that set. As an example, let's say that `james` is part of the `audi` group as can be seen in the following `/etc/group` line:

```
audi:x:1003:james,mary
```

The user `james` would like to issue the command `echo hello > /tmp/test.txt`. When processing the permissions above, Linux will check the `user` set first and see that `james` is in the `user` set for this file and apply the corresponding permissions. This means that `james` has no permissions over this file and his command would fail.

The user `mary` would like to issue the same command. This time, Linux will see that `mary` is not in the `user` set. It will then continue on to the `group` set and see that `mary` is part of the `audi` group and apply the corresponding permissions. This means that `mary` has `read`, `write`, and `execute` permissions over this file and her command would succeed.

The user `robert` would like to issue the same command. Linux will see that `robert` is not in the `user` set. It will then continue on to the `group` set and see that `robert` is not in the `audi` group either. This leaves `robert` in the `other` set and Linux will apply the corresponding permissions. This means that `robert` has the `read` permission over this file and his command would fail. He could, however, successfully issue the command `cat /tmp/test.txt`.
