# Set User ID (SUID) Complexities

#### When Applied to Interpreted Scripts

For security reasons, the `suid` bit is ineffective when applied to interpreted scripts vs compiled programs. This means that if the bash script `test.sh` has the `suid` bit set and is owned by `root`, you will never actually be able to take advantage of this bit to execute the script as `root`. If the same tasks the bash script would take are instead done via a compiled program, such as one written in `c`, the `suid` bit will be effective. You can play around with this concept in the [`Testing`](testing.md) portion of this material as I have included a bash script and a compiled c program which both

