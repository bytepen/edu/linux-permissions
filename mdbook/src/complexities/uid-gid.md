# User ID (UID) & Group ID (GID) Complexities

#### ID Reuse

Who is the user and group of the file in the following `ls -l` output?

```
-rwxrwx--- 1 james james 0 Mar 21 15:23 /home/james/test.txt
```

If you said `james` for both, you are correct. We can also see that they have `read`, `write`, and `execute` permissions and that `other` users shouldn't have any permissions. This, however, isn't always the case thanks to the ability to reuse User and Group ID's.

Check out the following lines from the `/etc/passwd` file:

```
james:x:1000:1000:James:/home/james:/bin/bash
mary:x:1001:1001:Mary:/home/mary:/bin/bash
hack3r:x:1000:1000:Hackerman:/home/hack3r:/bin/bash
```

Note that a third user has been created named `hack3r`. This user has the same User ID (`1000`) and Group ID (`1000`) as `james`, and as far as Linux Permissions are concerned, they both have full control over the file above. This is because the `ls -l` output above actually does a bit of name resolution for you and actually uses User and Group IDs, so it actually means:

```
-rwxrwx--- 1 1000 1000 0 Mar 21 15:23 /home/james/test.txt
```

This situation doesn't normally arise since user id's and group id's are normally incremented by 1 as can be seen with `mary`'s ID's of `1001`. However, this is easy enough to encounter since the following simple `useradd` command could be used to create the `hack3r` account above.

`useradd hack3r -ou 1000 -g 1000`

#### Supplementary Groups

In addition to the case above, another issue that can come into play is the fact that a user may be part of more than one group. Let's forget that `hack3r` was created for a moment and only examine `james` and `mary`. Both of these users have been created with all the defaults and are only part of one group that matches their name as shown in the following `/etc/passwd` and `/etc/group` lines:

```
james:x:1000:1000:James:/home/james:/bin/bash
mary:x:1001:1001:Mary:/home/mary:/bin/bash
```

```
james:x:1000:
mary:x:1001:
```

One day, the following command is issued and `mary` is added to the `james` group as shown in the updated `/etc/groups` entry:

```
usermod mary -aG james
```

```
james:x:1000:mary
```

This means that `mary` now has the permissions to do anything that the `james` group is able to do. In the example file given at the beginning of this section, `mary` now has `read`, `write`, and `execute` permissions for the file `/home/james/test.txt`
