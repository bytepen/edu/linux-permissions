# Complexities

Note that this isn't meant to be a comprehensive list of every edge case you may run into in regards to Linux Permissions. Instead, it is just a few that came to mind and that I felt were noteworthy. If you have any you would like to see added, feel free to let me know by opening an [Issue on GitLab](https://gitlab.com/bytepen/education/linux-permissions/-/issues).

I have broken situations which complicate permissions into the following sections:

* [Set User ID (`suid`)](./suid.md)
* [User/Group ID](./uid-gid.md)
