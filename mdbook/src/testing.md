# Testing

## Getting Started

Once you believe you have a grasp on Linux Permissions, I encourage you to check out the Docker Image I've put together by running the following command on any Docker enabled workstation.

`docker run -it bytepen/linux-permissions`

This will pull down and start a Docker Container that will generate a random set of files/directories and various users. It then sets random permissions on those files/directories.

#### Updating

It's possible that I've made changes to the Docker Image since you first used it. To make sure you have the latest version, run the following command from time to time:

`docker pull bytepen/linux-permissions`

## Commands

After you have been dropped into the Docker Container with the command above, you have several commands available to you.

* `q`: Generate a question such as `Can james delete /LP/random/orange?`
* `c`: Repeat the Current question
* `r`: Regenerate the initial file/directory structure and permissions/ownership
    * NOTE: If you run this command as another user, then you exit back to root, /LP/random may appear empty. Just re-run `r` or `cd /LP/random`

## Usage

The intended workflow is as follows:

1. Drop into the Docker Container
1. Run the command `q` to generate a question
1. Work through the question as shown in the [Example Question](#example-question) section
1. Run the `r` command to reset everything and the `q` command for a new question

### Example Question 

For example, if the question is `Can james execute /LP/random/charcoal/maroon/date.sh?`, you should do something along the following:

1. `su james`
1. `/LP/random/charcoal/maroon/date.sh`
1. Assuming it didn't execute, make as few changes as possible to the permissions so that the script will execute
    * You should avoid changing the ownership of files/directories
    * You can use `sudo chmod` to change permissions
1. Once it is able to execute, you should run the `r` command and make it execute again until you're certain you understand exactly why it didn't execute the first time and what needs to happen to make it execute.
