#include <stdio.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <sys/types.h>

void prt_user(struct passwd *pw) {
  printf("\tname=%s\n", pw->pw_name);
  printf("\tuid=%d\n", pw->pw_uid);
  printf("\tgid=%d\n\n", pw->pw_gid);
}

void prt_group(struct group *gr) {
  printf("\tname=%s\n", gr->gr_name);
  printf("\tgid=%d\n\n", gr->gr_gid);
}

int main () {
  /*struct passwd *pw;
  struct passwd *epw;

  int uid = getuid();
  pw = getpwuid(uid);
  int euid = geteuid();
  epw = getpwuid(euid);
  int gid = getgid();
  int egid = getegid();
  printf("%d:%d\n", getuid(), getgid());
  printf("%d:%d\n", geteuid(), getegid());*/
  printf("Current User:\n");
  prt_user(getpwuid(getuid()));
  printf("Current Group:\n");
  prt_group(getgrgid(getgid()));
  printf("Effective User:\n");
  prt_user(getpwuid(geteuid()));
  printf("Effective Group:\n");
  prt_group(getgrgid(getegid()));
  //printf("You are %s (%d:%d)\n", pw->pw_name, uid, gid);
  //printf("This was executed as %s (%d:%d)\n", epw->pw_name, euid, egid);
}

