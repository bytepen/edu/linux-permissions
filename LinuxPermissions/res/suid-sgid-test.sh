#!/bin/bash

if [ -z "${USER}" ]; then
    $USER="root"
fi

echo "Current User:"
echo -e "\tname=${USER}"
echo -e "\tuid=$(id -u)"
echo -e "\tgid=$(id -g)"
echo ""
echo "Current Group:"
echo -e "\tname=$(getent group $(id -g) | awk -F: '{print $1}')"
echo -e "\tgid=$(id -g)"
echo ""
echo "Effective User:"
echo -e "\tname=$(getent passwd ${EUID} | awk -F: '{print $1}')"
echo -e "\tuid=${EUID}"
echo -e "\tgid=$(getent group ${EUID} | awk -F: '{print $3}')"
echo ""
echo "Effective Group:"
echo -e "\tname=$(getent group ${EUID} | awk -F: '{print $1}')"
echo -e "\tgid=$(getent group ${EUID} | awk -F: '{print $3}')"
echo ""
echo "NOTE: Linux ignores suid & sgid bits for interpreted files like this script"
