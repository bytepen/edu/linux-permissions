#!/usr/bin/python3

import os
import shutil
import json


def create_item(item):
    if item['type'] == 'folder':
        os.makedirs(item['name'], exist_ok=True)
    elif item['type'] == 'elf':
        shutil.copyfile("/opt/LP/LinuxPermissions/res/suid-sgid-test", item['name'])
    elif item['type'] == 'sh':
        shutil.copyfile("/opt/LP/LinuxPermissions/res/suid-sgid-test.sh", item['name'])


def write_state(users, groups, items):
    os.makedirs("/etc/LP", exist_ok=True)
    with open("/etc/LP/state.json", "w+") as f:
        json.dump({
            "users": users,
            "groups": groups,
            "items": items
        }, f)


def read_state():
    with open("/etc/LP/state.json", "r") as f:
        return json.load(f)
