#!/usr/bin/python3

import random
import lputils.shared


class Question:
    def __init__(self):
        self.state = lputils.shared.read_state()

    def generate(self):
        entity = random.choice(self.state['users'])
        item = random.choice(self.state['items'])
        verb = ["delete", "move"]
        if item["type"] == "folder":
            verb += ["open"]
        elif item["type"] == "sh":
            verb += ["execute", "read"]
        elif item["type"] == "elf":
            verb += ["execute"]

        with open('/tmp/LP-Question.txt', 'w') as f:
            f.write("===========================================================================\n")
            f.write(f"  Question: Can {entity} {random.choice(verb)} {item['name']}?\n")
            f.write("===========================================================================")

    def ask(self):
        with open('/tmp/LP-Question.txt') as f:
            print(f.read())
