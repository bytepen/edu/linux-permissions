#!/usr/bin/python3

import argparse
import os
import pty
from lputils import *

class LinuxPermissions:
    def __init__(self):
        self.check_docker()
        self.parse_args()

    @staticmethod
    def check_docker():
        if not os.path.isfile('/etc/alpine-release'):
            print(
                "It would appear this script is not being run in a bytepen/linux-permissions Docker Container.\n" +
                "This script creates all sorts of random users and files.\n" +
                "To protect your system, please run this in a bytepen/linux-permissions Docker Container.\n\n" +
                "For example: 'docker run -it bytepen/linux-permission'\n")
            exit()

    def parse_args(self):
        parser = argparse.ArgumentParser()
        group = parser.add_mutually_exclusive_group()
        group.add_argument('-b', '--build', help="Builds default state for /LP/random", action="store_true")
        group.add_argument('-q', '--question', help="Ask a question about Permissions", action="store_true")
        group.add_argument('-c', '--current', help="Ask the Current Question Again", action="store_true")
        group.add_argument('-r', '--reset', help="Reset permissions", action="store_true")
        args = parser.parse_args()
        if args.question or os.path.abspath(__file__) == "/usr/local/bin/q":
            q = question.Question()
            q.generate()
            q.ask()
        elif args.current or os.path.abspath(__file__) == "/usr/local/bin/c":
            q = question.Question()
            q.ask()
        elif args.reset or os.path.abspath(__file__) == "/usr/local/bin/r":
            if os.geteuid() == 0:
                reset.Reset()
            else:
                print("Please run this command as root.\nExample:\n\t$ sudo r\n\t# r")
        elif args.build:
            build.Build()
            os.system('clear')
            self.print_welcome()
            q = question.Question()
            q.generate()
            q.ask()
            pty.spawn("/bin/bash")
        else:
            parser.print_help()

    @staticmethod
    def print_welcome():
        print("===========================================================================")
        print("Welcome!\n")
        print("This Docker Container has been made to go alongside my Linux Permissions")
        print("\twrite-up that can be found at the following URL:")
        print("\thttps://bytepen.gitlab.io/linux-permissions\n")
        print("Many users/groups have been created, as well as files/folders in /LP.")
        print("The contents of /LP/random have been randomly generated.")
        print("Play around with them to determine how permissions work in Linux.\n")
        print("As an example, the user 'james' has been created. Answer these questions:")
        print("\t- What folders can james create and modify? Why?")
        print("\t- What files can james create and modify? Why?\n\n")
        print("If you run into an action you can't perform, make as few changes as")
        print("\tpossible to make it so you can.\n\n")
        print("The following commands are available for assistance:")
        print("\t- q: Generate a Question")
        print("\t- c: Repeat the Current Question")
        print("\t- r: Restore Default State\n\n")
        print("NOTE: You can switch to james with the command 'su james'")
        print("===========================================================================")

if __name__ == "__main__":
    LinuxPermissions()
